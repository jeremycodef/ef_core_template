namespace EFLib.Models
{
    public class Client
    {
        public int Id {get;set;}
        public string ClientName {get;set;}
        public string ClientEmail {get; set;}
    }
}