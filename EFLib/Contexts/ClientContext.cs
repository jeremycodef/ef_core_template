using System;
using EFLib.Models;
using Microsoft.EntityFrameworkCore;

namespace EFLib.Contexts
{
    public class ClientContext: DbContext
    {
        public DbSet<Client> Clients {get; set;}
        public ClientContext(){

        } 
        protected override void OnConfiguring(DbContextOptionsBuilder options){
            options.UseNpgsql(@"Server=localhost;Port=5432;Database=EFTest;User Id=postgres;Password=TestPW");
            options.LogTo(Console.WriteLine);
        }
    }
}