﻿using System;
using System.Linq;
using EFLib.Contexts;
using EFLib.Models;

namespace EFDriver
{
    class Program
    {
        static void Main(string[] args)
        {
            using(var db = new ClientContext()){
                db.Add(new Client{ClientName="TestClient", ClientEmail="MyClient@gmail.com"});
                db.SaveChanges();

                Client client = db.Clients.First();
                Console.WriteLine($"Client queried: {client}");

                client.ClientEmail = "Updated@gmail.com";
                db.SaveChanges();
            }
            Console.WriteLine("Hello World!");
        }
    }
}
